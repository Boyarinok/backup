/**
 * @author Автор
 * @version Версия
 * @since 1.8T
 */

public class TestClass {
    /**
     * переменная типа Integer
     * @return Integer
     */
    private int perv;
    /**
     * переменная типа Double
     */
    private Double test;
    /**
     * переменная типа String
     */
    public String fasd;
    /**
     * переменная типа char
     * @param test it here
     */
    private Char som;
    /**
     * нов класс с переменными: lkdsa - 10, r - 7, polk=number, siLc=12345
     */
    public Progtest() {
        lkdsa = 1;
        r = 1.7;
        polk="654";
        siДс="ы";
    }
    /**
     * инициализация переменных c помощью конструктора
     */
    public Progtest(int perv, Double vtor, String imya, Char simv) {
        this.perv = perv;
        this.vtor = vtor;
        this.imya = imya;
        this.simv = simv;
    }
    /**
     * для получения lkdsa
     * @return Int
     */
    public int r() {
        return r;
    }
    /**
     * для установки r
     * @param r Int
     */
    public void siLc(int r) {
        this.r = r;
    }
}

